//64.Write a program to implement Queue operations using Linked List
#include<stdio.h>
#include<stdlib.h>
struct node{
	int data;
	struct node *next;
};
typedef struct node nd;
nd *front=NULL,*rear=NULL;
void insert()
{
	nd *temp;
	int item;
	temp=(nd*)malloc(sizeof(nd));
	printf("\nEnter the item.\n");
	scanf("%d",&item);
	temp->data=item;
	temp->next=NULL;
	if(rear==NULL)
	{
		front=temp;
		rear=temp;
	}
	else
	{
		rear->next=temp;
		rear=temp;
	}
}
void del()
{
	int item;
	if(front!=NULL)
	{
		item=front->data;
		if(front==rear)
		{
		printf("The item deleted is:%d",item);
		front=NULL;
		rear=NULL;
		}
		else
		{		
		front=front->next;
		printf("The item deleted is:%d",item);
		}
    }
    else
    {
    	printf("\nqueue is empty.");
	}
}
void traverse()
{
	nd *temp;
	temp=front;
	if(front!=NULL)
	{
		printf("\nCurretly queue has:");
		while(temp!=NULL)
		{
			printf("%d ",temp->data);
			temp=temp->next;
		}
	}
	else
	printf("\nQueue is empty.");
}
void main()
{
	int choice;
	do
	{
		printf("\n1.Insert\n2.Delete\n3.Traverse\n4.Exit");
		printf("\nEnter your choice.\n");
		scanf("%d",&choice);
		switch(choice){
			case 1:{
				insert();
				break;
			}
			case 2:{
				del();
				break;
			}
			case 3:{
				traverse();
				break;
			}
			case 4:{
				exit(0);
				break;
			}
		}
	}while(choice!=4);
}

