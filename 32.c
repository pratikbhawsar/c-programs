//32.Wap for Matrix multiplication by checking compatibility.
#include<stdio.h>
void main()
{
	int a[10][10],b[10][10],res[10][10],row1,col1,row2,col2;
	printf("Enter the no. of rows in first matrix.\n");
	scanf("%d",&row1);
	printf("Enter the no. of columns in first matrix.\n");
	scanf("%d",&col1);
	printf("Enter the no, of rows in second matrix.\n");
	scanf("%d",&row2);
	printf("Enter the no. of columns in second matrix.\n");
	scanf("%d",&col2);
	if(col1!=row2)
	{
		printf("The matrix multiplication is not compatible.");
	}
	else if(col1==row2)
	{
		printf("Enter the elements of first matrix.\n");
		for(int i=0;i<row1;i++)
		{
			for(int j=0;j<col1;j++)
			{
				scanf("%d",&a[i][j]);
			}
		}
		printf("\nEnter the elements of second matrix.\n");
		for(int i=0;i<row2;i++)
		{
			for(int j=0;j<col2;j++)
			{
				scanf("%d",&b[i][j]);
			}
		}
		for(int i=0;i<row1;i++)
		{
			for(int j=0;j<col2;j++)
			{
				res[i][j]=0;
				for(int k=0;k<row2;k++)
				{
					res[i][j]+=a[i][k]*b[k][j];
				}
			}	
		}
        printf("\nMatrix multiplication is:\n");
		for(int i=0;i<row1;i++)
		{
			for(int j=0;j<col2;j++)
			{
				printf(" %d ",res[i][j]);
			}
		  printf("\n");
		}
	}
}























