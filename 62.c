//62.Write a program to implement Queue operations using Arrays.
#include<stdio.h>
#include<stdlib.h>
int queue[5],rear=-1,front=-1;
void main()
{
	int choice;
	int item;
	do
	{
		printf("\n1.Insert \n2.Delete \n3.Display queue. \n4.Exit.\nEnter your choice.\n");
		scanf("%d",&choice);
		switch(choice){
			case 1:{				
				if(rear<4)
				{
					printf("\nEnter the element to be inserted.\n");
					scanf("%d",&item);
					if(rear==-1)
					{
						front=0;
						rear=0;
						queue[rear]=item;	
					}		
					else
					{
						rear=rear+1;
						queue[rear]=item;
					}
				}
				else
				{
					printf("\nQueue is full.\n");
				}
				break;
			}
			case 2:{
				if(front!=-1)
				{	
				    item=queue[front];				
					if(front==rear)
					{
						front=-1;
						rear=-1;
					}
					else
					front=front+1;
					printf("\nThe item deleted is:%d\n",item);					
				}
				else
				{
					printf("\nQueue is empty.\n");
				}
				break;
			}
			case 3:{
				if(front!=-1)
				{
					if(rear>=front)
					{
						printf("\nCurrently queue has:");
						for(int i=rear;i>=front;i--)
						{
					   		printf("%d ",queue[i]);
						}
					}
					else
					{
						printf("\nQueue is empty.\n");
					}
				}
				else
				{
					printf("\nQueue is empty.\n");
				}
				
				break;
			}
			case 4:{
				exit(0);
				break;
			}
		}
	}while(choice!=4);	
}
