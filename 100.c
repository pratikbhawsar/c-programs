//100.Write a c program to convert decimal number to octal number.
#include<stdio.h>
void main()
{
	int rem,decno,octno[10],i=0;
	printf("Enter decimal number.\n");
	scanf("%d",&decno);
	while(decno!=0)
	{
		rem=decno%8;
		octno[i]=rem;
		decno=decno/8;
		i++;
	}
	printf("Octal is:");
	for(int j=i-1;j>=0;j--)
	{
		printf("%d",octno[j]);
	}
}
