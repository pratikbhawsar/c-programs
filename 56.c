//56.Write a program to count the number of words, lines and characters in the given file.
#include<stdio.h>
void main()
{
	FILE *fi;
	char str[500],fname[50];
	int i=0,words=1,lines=1,ch=0;
	printf("Enter the name of file to be read.\n");
	gets(fname);
	fi=fopen(fname,"r");
	if(fi)
	{
		do 
	    {
	    	str[i]=fgetc(fi);
	    	if(str[i]==32||str[i]==10)
			words++;
	    	if(str[i]==10)
			lines++;
			else
			ch++;
			i++;
		}
		while(str[i]!=EOF);
		printf("Characters=%d, words=%d, lines=%d",ch,words,lines);
	}
	else
	{
		printf("File not found.");
	}
}
