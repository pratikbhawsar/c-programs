//99.C program to convert each digits of a number in words
#include<stdio.h>
void main()
{
	unsigned long long int no;
	int rem,i=0;
	char *words[1000];
	printf("Enter the no.\n");
	scanf("%ld",&no);
	while(no!=0)
	{
		rem=no%10;
		switch(rem)
		{
		   case 1: words[i++]="one ";   break;
		   case 2: words[i++]="two ";   break;
		   case 3: words[i++]="three "; break;
		   case 4: words[i++]="four ";  break;
		   case 5: words[i++]="five ";  break;
		   case 6: words[i++]="six ";   break;
		   case 7: words[i++]="seven "; break;
		   case 8: words[i++]="eight "; break;
		   case 9: words[i++]="nine ";  break;
		   case 0: words[i++]="zero ";  break;
		}
		no=no/10;		
	}
	for(int j=i-1;j>=0;j--)
	{
		printf("%s",words[j]);
	}
}
