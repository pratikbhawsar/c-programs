/* 49.Write a C program using structure to create a library catalogue 
with the following fields;
Access number, author's name. Title of the book,
year of publication, publisher's name, price.*/
#include<stdio.h>
#include<stdlib.h>
void main()
{
	int no;
	struct libcat{
		int access_no;
		char auth_name[30];
		char title[30];
		int year;
		char pub_name[30];
		int price;
	};
	struct libcat *p;
	printf("Enter the no. of books in library.\n");
	scanf("%d",&no);
	p=(struct libcat*)malloc(no*sizeof(struct libcat));
	for(int i=0;i<no;i++)
	{	
	    printf("\nFor book %d:\n",i+1);
		printf("Enter Access number\n");
		scanf("%d",&(p+i)->access_no);
		printf("Enter author's name\n");
		scanf("%s",&(p+i)->auth_name);
		printf("Enter Title of the book\n");
		scanf("%s",&(p+i)->title);
		printf("Enter year of publication\n");
		scanf("%d",&(p+i)->year);
		printf("Enter publisher's name\n");
		scanf("%s",&(p+i)->pub_name);
		printf("Enter price\n");
		scanf("%d",&(p+i)->price);
    }
    printf("\nThe library catalogue has following entries.\n");
    for(int i=0;i<no;i++)
	{	
		printf("\nAccess number:%d\n",(p+i)->access_no);
		printf("Author's name: %s\n",(p+i)->auth_name);
		printf("Enter Title of the book:%s\n",(p+i)->title);
		printf("Year of publication:%d\n",(p+i)->year);
		printf("Publisher's name:%s\n",(p+i)->pub_name);
		printf("Price:%d\n\n",(p+i)->price);
    }
}
