//36.Write a function which takes a string as an argument and return its length.(using pointers).
#include<stdio.h>
int lenfunc(char a[100])
{
	char *p;
	int len=0;
	p=&a[0];
	while(*(p+len)!='\0')
	len++;
	return len;
}
void main()
{
	char str[100];
	int len=0;
	printf("Enter the string.\n");
	gets(str);
	len=lenfunc(str);
	printf("The length of the string is %d",len);
}

