//91.Write a c program to solve quadratic equation.
#include<stdio.h>
#include<math.h>
void main()
{
	float a,b,c,dtr,real,img;
	printf("Enter a, b and c of quadratic equation.\n");
	scanf("%f%f%f",&a,&b,&c);
	dtr=b*b-4*a*c;
	if(dtr>0)
	{
		printf("Roots are real and different:\n");
		printf("%.2f, %.2f",(-b+sqrt(dtr))/(2*a),(-b-sqrt(dtr))/(2*a));
	}
	if(dtr==0)
	{
		printf("Roots are real and equal:\n");
		printf("%.2f, %.2f",(-b)/(2*a),(-b)/(2*a));
	}
	if(dtr<0)
	{
		printf("Roots are complex and different.\n");
		real=-b/(2*a);
		img=sqrt(-dtr)/(2*a);
		printf("%.2f+%.2fi, %.2f-%.2fi",real,img,real,img);
	}
}
