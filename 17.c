//17.Write a program to check the given year is leap year or not.

#include<stdio.h>
#include<conio.h>
int main()
{
    int year;
    printf("Enter the year: \n");
    scanf("%d",&year);
    if(year<100)
    {
        if(year%4==0)
        printf("Entered year is a leap year.");
        else
        printf("Entered year is not a leap year.");
    }
    if(year>=100&&year<400)
    {
        if(year%4==0&&!(year%100==0))
        printf("Entered year is a leap year.");
        else
        printf("Entered year is not a leap year.");
    }
    if(year>=400)
    {
        if(year%4==01&&year%400==0)
        printf("Entered year is a leap year.");
        else
        printf("Entered year is not a leap year.");
    }
    return 0;
}
