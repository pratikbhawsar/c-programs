//82.Write a c program to find out largest element of an array.
#include<stdio.h>
#include<conio.h>
int main()
{
    int a[50],no,max;
    printf("Enter the numbers of elements. \n");
    scanf("%d",&no);
    printf("Enter the elements.\n");
    for(int i=0;i<no;i++)
    {
        scanf("%d",&a[i]);
    }
    max=a[0];
    printf("Elements are:\n");
    for(int i=0;i<no;i++)
    {
    printf("%d ",a[i]);
    }
    printf("\n");
    for(int i=0;i<no;i++)
    {
        if(a[i]>max)
        {
            max=a[i];
        }
    }
    printf("The largest element of array is : %d",max);
    return 0;
}
