//77.C program to find inverse of a matrix.
#include<stdio.h>
void main()
{
	int a[3][3],i,j;
	float dtr=0;
	 
	printf("Enter the elements of matrix: ");
	for(i=0;i<3;i++)
	  {
	      for(j=0;j<3;j++)
	        {
	           scanf("%d",&a[j][i]);
			}
	  }
	printf("\nThe matrix is\n");
	for(i=0;i<3;i++)
	  {
	    printf("\n");
	      for(j=0;j<3;j++)
	      {
	      	printf("%d\t",a[i][j]);
		  }
	  }
	for(i=0;i<3;i++)
	    {
	      	dtr = dtr + (a[0][i]*(a[1][(i+1)%3]*a[2][(i+2)%3] - a[1][(i+2)%3]*a[2][(i+1)%3]));
		}
	printf("\nInverse of matrix is: \n\n");
	   for(i=0;i<3;i++){
	      for(j=0;j<3;j++)
	       {
	           printf("%.2f\t",((a[(i+1)%3][(j+1)%3]*a[(i+2)%3][(j+2)%3])-(a[(i+1)%3][(j+2)%3]*a[(i+2)%3][(j+1)%3]))/dtr);
		   }
	       printf("\n");
	   }
}
