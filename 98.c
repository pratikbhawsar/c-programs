//98.Write a c program to convert octal number to hexadecimal number.
#include<stdio.h>
#include<math.h>
void main()
{
	int rem,octno,decno=0,i=0,j=0;
	char hexno[16];
	printf("Enter the octal no.\n");
	scanf("%d",&octno);
	while(octno!=0)
	{
		rem=octno%10;
		decno=decno+rem*pow(8,i);
		i++;
		octno=octno/10;
	}
    while(decno!=0)
    {
    	rem=decno%16;
    	if(rem>9)
    	hexno[j]=55+rem;
    	else 
    	hexno[j]=48+rem;
    	j++;
    	decno=decno/16;
	}
	printf("Hexadecimal is:");
	for(int k=j-1;k>=0;k--)
	{
		printf("%c",hexno[k]);
	}
}
