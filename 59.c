//59.Write a C program to open a pre-existing file and add information at the end of file. 
//Display the contents of the file before and after appending .
#include<stdio.h>
void main()
{
	FILE *fi;
	char str[600],strnew[600],text[100],fname[50];
	int i=0,j=0;
	printf("Enter the file name.\n");
	gets(fname);
	fi=fopen(fname,"r");
	do
	{
		str[i]=fgetc(fi);
		i++;
	}while(str[i]!=EOF);
	fclose(fi);
	fi=fopen(fname,"a");
	printf("\nBefore appending :\n%s\n---------------------------",str);
	printf("\nEnter information to add.\n");
	gets(text);
	fputs(text,fi);
	fclose(fi);
	fi=fopen(fname,"r");
	do
	{
		strnew[j]=fgetc(fi);
		j++;
	}while(strnew[j]!=EOF);
	printf("\nAfter appending :\n%s",strnew);
}
