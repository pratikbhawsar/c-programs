//23.Write a function to sort 1d integer array using Selection sort.
#include<stdio.h>
void main()
{
	int a[100],len,min,temp;
	printf("Enter the no. of elements.\n");
	scanf("%d",&len);
	printf("Enter the elements.\n");
	for(int i=0;i<len;i++)
	{
		scanf("%d",&a[i]);
	}
	printf("\nSorted array.\n");
	for(int i=0;i<len-1;i++)
	{
		min=i;
		for(int j=i+1;j<len;j++)
		{
			if(a[j]<a[min])
	       {
          	  min=j; 
	   	   }
		}	
		 temp=a[i];
		 a[i]=a[min];
		 a[min]=temp;
		 
	}
	for(int i=0;i<len;i++)
	{
		printf("%d ",a[i]);
	}
}

