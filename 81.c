//81.C program to find determinant of a matrix.
#include<stdio.h>
#include<stdlib.h>
void main()
{
	int a[3][3],choice,dtr,d1,d2,d3;
	printf("Enter the choice.\n1. 2X2 matrix\n2.3X3 matrix.\n");
	scanf("%d",&choice);
	switch(choice){
		case 1:{
			printf("Enter elements of 2X2 matrix.\n");
			for(int i=0;i<2;i++)
			{
				for(int j=0;j<2;j++)
				{
					printf("Element a[%d][%d]:",i+1,j+1);
			     	scanf("%d",&a[i][j]);		
				}
			}
			dtr=a[0][0]*a[1][1]-a[1][0]*a[0][1];
			printf("Determinant of 2X2 matrix is %d",dtr);
			break;
		}
		case 2:{
			printf("Enter elements of 3X3 matrix.\n");
			for(int i=0;i<3;i++)
			{
				for(int j=0;j<3;j++)
				{
					printf("Element a[%d][%d]:",i+1,j+1);
			     	scanf("%d",&a[i][j]);		
				}
			}
			d1=a[1][1]*a[2][2]-a[2][1]*a[1][2];
			d2=a[1][0]*a[2][2]-a[2][0]*a[1][2];
			d3=a[1][0]*a[2][1]-a[2][0]*a[1][1];
			dtr=a[0][0]*d1-a[0][1]*d2+a[0][2]*d3;
			printf("Determinant of 3X3 matrix is:%d ",dtr);
			break;
		}
		default:{
			exit(0);
			break;
		}
	}
	
}
