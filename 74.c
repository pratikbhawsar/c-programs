//74.Write a c program to find out sum of diagonal element of a matrix.
#include<stdio.h>
void main()
{
	int a[10][10],row,col,sum1=0,sum2=0;
	printf("Enter the no. of rows in matrix.\n");
	scanf("%d",&row);
	printf("Enter the no. of columns in matrix.\n");
	scanf("%d",&col);
	printf("Enter the elements.\n");
	for(int i=0;i<row;i++)
	{
		for(int j=0;j<col;j++)
		{
			scanf("%d",&a[i][j]);
		}
	}
	for(int i=0;i<row;i++)
	{
		for(int j=0;j<col;j++)
		{
			if(i==j)
			sum1+=a[i][j];
			if((i+j)==(col-1))
			sum2+=a[i][j];
		}
	}
	printf("Matrix is:\n");
	for(int i=0;i<row;i++)
	{
		for(int j=0;j<col;j++)
		{
			printf(" %d ",a[i][j]);
		}
	  printf("\n");
	}
	printf("Sum of first diagonal elements is %d.\n",sum1);
	printf("Sum of second diagonal elements is %d.",sum2);
	
}
