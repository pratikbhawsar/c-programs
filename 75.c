//75.Write a c program to find out transport of a matrix.
#include<stdio.h>
void main()
{
	int a[10][10],b[10][10],row,col;
	printf("Enter the no. of rows.\n");
	scanf("%d",&row);
	printf("Enter no. of columns.\n");
	scanf("%d",&col);
	printf("Enter the elements of matrix.\n");
	for(int i=0;i<row;i++)
	{
		for(int j=0;j<col;j++)
		{
			scanf("%d",&a[i][j]);
		}
	}
	for(int i=0;i<row;i++)
	{
		for(int j=0;j<col;j++)
		{
			b[j][i]=a[i][j];
		}
	}
	printf("\nTranspose of matrix is:\n");
	for(int i=0;i<row;i++)
	{
		for(int j=0;j<col;j++)
		{
			printf(" %d ",b[i][j]);
		}
	  printf("\n");
	}
}
