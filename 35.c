//35.Write a program to copy the string into another string using pointers.
#include<stdio.h>
void main()
{
	char str1[100],str2[100];
	char *ptr;
	int len=0;
	printf("Enter first string.\n");
	gets(str1);
	ptr=&str1[0];
	while(str1[len]!='\0')
	len++;
	for(int i=0;i<len;i++)
	{
		str2[i]=*(ptr+i);
	}
	printf("String two:\n");
	puts(str2);
}
