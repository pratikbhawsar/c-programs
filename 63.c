//63.Write a program to implement stack operations using Linked List.
#include<stdio.h>
#include<stdlib.h>
struct stack{
	int data;
	struct stack *next;
}*top=NULL;
typedef struct stack nd;
void push(int item)
{
	nd *temp;
	temp=(nd*)malloc(sizeof(nd));
	temp->data=item;
	temp->next=top;
	top=temp;
}
void pop()
{
	if(top==NULL)
	{
		printf("stack is empty.\n");
	}
	else
	{
		printf("Item deleted is %d ",top->data);
		top=top->next;
	}
}
void traverse()
{
    nd *temp;
    temp=top;
    if(temp!=NULL)
    {
    	printf("Currently stack has:");
	    while(temp!=NULL)
	    {
	    	printf("%d ",temp->data);
	    	temp=temp->next;
		}
	}
	else
	{
		printf("stack is empty.\n");
	}
}
void main()
{
	int choice,item;
	do
	{
		printf("\n1.push\n2.pop\n3.traverse\n4.exit");
		printf("\nEnter your choice.\n");
		scanf("%d",&choice);
		switch(choice){
			case 1:{
				printf("\nEnter the item.\n");
				scanf("%d",&item);
				push(item);
				break;
			}
			case 2:{
				pop();
				break;
			}
			case 3:{
				traverse();
				break;
			}
			case 4:
				exit(0);
		}	
	}while(choice!=4);
}
