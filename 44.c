//44.Write a program to illustrate usage of automatic,globel,extern,static and register variables.
#include<stdio.h>
extern int e;
int global;
int add(int a,int b)
{
	auto int c; //Here c is an auto variable.
	c=a+b;
	global++;
	return c;
}
int stat()
{
    static int i=0;
	i++;
	return i;
}
int e=45; //This is defined after declaring with extern keyword.
void main()
{
	register int reg;
    int a,b,c;	
	auto int result;
    result=add(5,8);
	printf("%d //Here result is an auto variable.",result);   //Here result is an auto variable.
	printf("\n%d // This is printed after declaring with extern keyword.",e);  // Extern keyword.
	global++;
	++global;
	printf("\n%d //This is global variable. ",global);// one increment from add function.
    a=stat();
    b=stat();
	printf("\n%d %d //This is example of static variable.",a,b);
	c=reg;
	printf("\n%d //This is example of register variable.(Default value)",c);
    	
}

