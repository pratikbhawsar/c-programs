//87.Write a c program for insert an element at desired position in an array.
#include<stdio.h>
int main()
{
    int a[100],len,pos,no;
    printf("Enter the number of elements in array.\n");
    scanf("%d",&len);
    printf("Enter the elements.\n");
    for(int i=0;i<len;i++)
    {
        scanf("%d",&a[i]);
    }
    printf("Elements are: \n");
    for(int i=0;i<len;i++)
    {
        printf("%d ",a[i]);
    }
    printf("\nEnter the new element.\n");
    scanf("%d",&no);
    printf("Enter the desired position for the new element.\n ");
    scanf("%d",&pos);
    --pos;
    printf("\n");
    for(int i=len;i>=pos;i--)
    {
        a[i+1]=a[i];
    }
    a[pos]=no;
    len++;
    printf("Elements are:\n");
    for(int i=0;i<len;i++)
    {
        printf("%d ",a[i]);
    }
}
