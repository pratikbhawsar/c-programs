//86.Write a c program for delete an element at desired position in an array.
#include<stdio.h>
void main()
{
    int a[100],len,pos;
    printf("Enter the no. of elements. \n");
    scanf("%d",&len);
    printf("Enter the elements.\n");
    for(int i=0;i<len;i++)
    {
        scanf("%d",&a[i]);
    }
    for(int i=0;i<len;i++)
    {
        printf("%d ",a[i]);
    }
    printf("\nEnter the position of element you want to delete.\n");
    scanf("%d",&pos);
    --pos;
    for(int i=pos;i<len-1;i++)
    {
        a[i]=a[i+1];
    }
    --len;
    printf("\n Elements are:\n ");
    for(int i=0;i<len;i++)
    {
        printf("%d ",a[i]);
    }
}
