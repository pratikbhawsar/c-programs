//53.Write a program to illustrate unions.
#include<stdio.h>
void main()
{
	union emp{
		int no;
		char year[2];
	}e1;
	e1.year[0]=5;	
	e1.year[1]=10;
	printf("%d,%d,%d",e1.no,e1.year[0],e1.year[1]);
	//Here output is 2565,5,10 
	//because memory is allocated only of two bytes.
	//2565 is equivalent to 10 & 5 together in binary(2 byte) i.e|00001010|00000101|
}
