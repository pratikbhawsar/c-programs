//78. Lower triangular matrix in c
#include<stdio.h>
void main()
{
	int a[10][10],row,col;
	printf("Enter the no. of rows in the matrix.\n");
	scanf("%d",&row);
	printf("Enter the no. of columns in the matrix.\n");
	scanf("%d",&col);
	printf("Enter the elements.\n");
	for(int i=0;i<row;i++)
	{
		for(int j=0;j<col;j++)
		{
			scanf("%d",&a[i][j]);
		}
	}
	printf("Matrix is:\n");
	for(int i=0;i<row;i++)
	{
		for(int j=0;j<col;j++)
		{
			printf(" %d ",a[i][j]);
		}
	  printf("\n");
	}
   	for(int i=0;i<row;i++)
   	{
   	 	for(int j=0;j<col;j++)
		{
		     if(!(i>=j))
		     {
		     	a[i][j]=0;
			 }
		}
	}
	printf("Lower triangular matrix is:\n");
	for(int i=0;i<row;i++)
	{
		for(int j=0;j<col;j++)
		{
			printf(" %d ",a[i][j]);
		}
	  printf("\n");
	}
		 
}
