//72.Write a c program for subtraction of two matrices
#include<stdio.h>
void main()
{
    int a[10][10],b[10][10],res[10][10],row,col;
    printf("Enter the number of rows.\n");
    scanf("%d",&row);
    printf("Enter the number of columns.\n");
    scanf("%d",&col);
    printf("Enter the elements of first matrix.\n");
    for(int i=0;i<row;i++)
    {
        for(int j=0;j<col;j++)
        {
            scanf("%d",&a[i][j]);
        }
    }
    printf("\nEnter the elements of second matrix.\n");
    for(int i=0;i<row;i++)
    {
        for(int j=0;j<col;j++)
        {
            scanf("%d",&b[i][j]);
        }
    }
    for(int i=0;i<row;i++)
    {
        for(int j=0;j<col;j++)
        {
            res[i][j]=a[i][j]-b[i][j];
        }
    }
    printf("\nSubstraction of matrices is:\n");
    for(int i=0;i<row;i++)
    {
        for(int j=0;j<col;j++)
        {
            printf(" %d ",res[i][j]);
        }
        printf("\n");
    }
}
