//31.Write a C program to find the sum of individual digits of a positive integer.
#include<stdio.h>
#include<conio.h>
void main()
{
    int no,sum=0,rem;
    printf("Enter the number.\n");
    scanf("%d",&no);
    while(no!=0)
    {
        rem=no%10;
        sum=sum+rem;
        no=no/10;       
    }
    printf("The sum of all the digits of number is: %d",sum);
}

