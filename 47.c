/*47.	Define a structure with the name student which contains sno,sname,marks.
WAP to read and n students information and print the details of the students
whose marks greater than or equal to average marks of the students.*/
#include<stdio.h>
void main()
{
	int no,avg,sum=0;
	struct student{
		int sno;
		char sname[30];
		int marks;
	};
	struct student *ptr;
	printf("Enter the no. of students.\n");
	scanf("%d",&no);
	ptr=(struct student*)malloc(no*sizeof(struct student));
	for(int i=0;i<no;i++)
	{
		printf("Enter the Sno, Sname and marks.\n");
		scanf("%d%s%d",&(ptr+i)->sno,&(ptr+i)->sname,&(ptr+i)->marks);
	}
	for(int i=0;i<no;i++)
	{
	    sum=sum+((ptr+i)->marks);
	}
	avg=sum/no;
	printf("\nAverage Marks:%d\n",avg);
	printf("Details of students whose marks are greater than or equal to average marks.\n");
	for(int i=0;i<no;i++)
	{
		if(((ptr+i)->marks)>=avg)
		printf("\nStudent:\n\nSno-%d\nName-%s\nMarks-%d\n\n",(ptr+i)->sno,(ptr+i)->sname,(ptr+i)->marks);
	}
}
