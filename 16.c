//16.Write a program to swap two values without using temporary variable.
#include<stdio.h>
#include<conio.h>
int swap(int a,int b)
{
    a=a+b;
    b=a-b;
    a=a-b;
    printf("After swapping: %d and %d.",a,b);
}
 int main()
 {
     int a,b;
     printf("Enter two numbers.\n");
     scanf("%d%d",&a,&b);
     printf("Before swapping: %d and %d.\n",a,b);
     swap(a,b);
     return 0;
 }
