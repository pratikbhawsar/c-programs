//65.Write a program to perform Singly Linked List Operations(create,insert,delete,traversing).
#include<stdio.h>
#include<stdlib.h>
struct node{
	int data;
	struct node *next;
};
typedef struct node nd;
nd *start=NULL,*trav;
void insert()
{
	nd *temp,*end;
	int item;
	temp=(nd*)malloc(sizeof(nd));
	printf("Enter the item.\n");
	scanf("%d",&item);
	temp->data=item;
	temp->next=NULL;
	if(start==NULL)
	{
		start=temp;
	}
	else 
	{
		end=start;
		while(end->next!=NULL)
		{
			end=end->next;
		}
		end->next=temp;
	}
	
}
void insertbegin()
{
	nd *temp;
	int item;
	temp=(nd*)malloc(sizeof(nd));
	printf("Enter the item.\n");
	scanf("%d",&item);
	temp->data=item;
	if(start==NULL)
	temp->next=NULL;
	else
	temp->next=start;
	start=temp;	
}
void del()
{
	nd *temp,*end;
	if(start==NULL)
	{
		printf("\nList is empty.\n");
	}
	else if((start)->next==NULL)
	{
		start=NULL;
	}
	else
	{
		end=start;
		temp=start->next;
		while(temp->next!=NULL)
		{
			end=temp;
			temp=temp->next;
		}
		end->next=NULL;
	}
}
void delbegin()
{
	if(start==NULL)
	printf("\nList is empty.\n");
	else
	start=start->next;
}
void traverse()
{
	trav=start;
	printf("\nList has:");
	while(trav!=NULL)
	{
	printf("%d ",trav->data);
	trav=trav->next;
	}
}
void main()
{
	int choice;	
	do
	{
		printf("\n1.Insert\n2.Insert at begining\n3.delete\n4.Delete from begining.\n5.Traverse.\n6.exit\n");
		printf("enter your choice.\n");
		scanf("%d",&choice);
		switch(choice){
			case 1:{
				insert();
				break;
			}
			case 2:{
				insertbegin();
				break;
			}
			case 3:{
				del();
				break;
			}
			case 4:{
				delbegin();
				break;
			}
			case 5:{
				traverse();
				break;
			}
			case 6:{
				exit(0);
				break;
			}
		}
	}
	while(choice!=6);
}

