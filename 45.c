//45.Write a program to find the sum of 1 d integer array(using malloc()).
#include<stdio.h>
void main()
{
	int *p,no,sum=0;
	printf("Enter the number of elements in array.\n");
	scanf("%d",&no);
	printf("Enter the numbers.\n");
	p=(int *)malloc(no*sizeof(int));
	for(int i=0;i<no;i++)
	{
		scanf("%d",p+i);
	}
	for(int i=0;i<no;i++)
	{
		sum=sum+*(p+i);
	}
	printf("\nElements are:\n");
	for(int i=0;i<no;i++)
	{
		printf("%d ",*(p+i));
	}
    printf("\nSum of array elements is:%d",sum);
}
