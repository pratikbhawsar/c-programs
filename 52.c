/* 52 . Write a function to copy the contents of one student into another student record Of same.*/
#include<stdio.h>
#include<string.h>
struct student{
	int rollno;
	char sname[30];
	int marks;
}s1,s2;
void main()
{
	struct student copy(struct student);
	printf("Enter roll no.\n");
	scanf("%d",&s1.rollno);
	printf("Enter student name.\n");
	scanf("%s",&s1.sname);
	printf("Enter marks.\n");
	scanf("%d",&s1.marks);
    s2=copy(s1);
	printf("Student roll no:%d\n",s2.rollno);
	printf("Student name:%s\n",s2.sname);
    printf("Student marks:%d\n",s2.marks);		
}
struct student copy(struct student a)
{
	struct student temp;
	temp.rollno=a.rollno;
	strcpy(temp.sname,a.sname);
	temp.marks=a.marks;
	return temp;
}
