//43.Wap to check the given matrix is symmetric or not.
#include<stdio.h>
void main()
{
	int a[10][10],row,col,flag=1;
	printf("Enter the no. of rows.\n");
	scanf("%d",&row);
	printf("Enter the no. of columns.\n");
	scanf("%d",&col);
	if(row==col)
	{
		printf("Enter the elements of matrix.\n");
	    for(int i=0;i<row;i++)
		{
			for(int j=0;j<col;j++)
			{
				scanf("%d",&a[i][j]);
			}	
		}
		printf("Matrix is:\n");
		for(int i=0;i<row;i++)
		{
			for(int j=0;j<col;j++)
			{
				printf(" %d ",a[i][j]);
			}	
		  printf("\n");
		}
		for(int i=0;i<row;i++)
		{
			for(int j=0;j<col;j++)
			{
				if(!(a[i][j]==a[j][i]))
				{
					flag=0;
				}	
			}	
		}
		if(flag==1)
		{
			printf("Matrix is symmetrical.");
		}
		else
		{
			printf("Matrix is not symmetrical");
		}	
	}
	else 
	{
	   printf("Matrix is not a square matrix.");
	}
}
