//57.	Write a program to print the data of the file onto the monitor in reverse order.
#include<stdio.h>
void main()
{
	FILE *fi;
	char str[500],fname[50];
	int i=0;
	printf("Enter the name of file.\n");
	gets(fname);
	fi=fopen(fname,"r");
	while(str[i]!=EOF)
	{
		str[i]=fgetc(fi);
		i++;
	}
	for(int j=0,k=i,temp;j<k;j++,k--)
	{
		temp=str[j];
		str[j]=str[k];
		str[k]=temp;
	}
	printf("%s",str);
}
