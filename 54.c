//54. Explaining pointer to a structure variable with example.
#include<stdio.h>
void main()
{
	struct book{
		char title[20];
		int year;
		int price;
	}s1;
	struct book *p;
    p=&s1;
    printf("Enter title,year and price of book.\n");
    scanf("%s%d%d",&p->title,&p->year,&p->price);
    printf("\n Title:%s\n Year:%d \n Price:%d",p->title,p->year,p->price);   
}
