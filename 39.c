//39.Write a program to calculate mn value using do-while loop.
#include<stdio.h>
void main()
{
	int m,n,res=1;
	printf("Enter the value of m and n.\n");
	scanf("%d%d",&m,&n);
	if(n==0)
	{
		printf("Result is 1.");
	}
	else
	{
		do
		{
			res=res*m;
			n--;
		}
		while(n!=0);
		printf("Result is %d.",res);	
	}		
}
