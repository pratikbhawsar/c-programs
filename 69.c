//69.Write a Function to combine two singly linked lists.
#include<stdio.h>
struct node{
	int data;
	struct node *next;
};
typedef struct node nd;
nd *start1=NULL,*start2=NULL;
nd* insertend(nd *start)
{
	int item;
	nd *temp,*end;
	temp=(nd*)malloc(sizeof(nd));
	printf("\nEnter the data.\n");
	scanf("%d",&item);
	temp->data=item;
	temp->next=NULL;
	if(start==NULL)
	{
		start=temp;
	}
	else
	{
		end=start;
		while(end->next!=NULL)
		{
			end=end->next;
		}
		end->next=temp;
	}
	return start;
}
void traverse(nd *start)
{
	nd *temp;
	if(start==NULL)
	{
		printf("List is empty.\n");
	}
	else
	{		
		temp=start;
		printf("\nCurrently list has:\n");
		while(temp!=NULL)
		{
			printf("%d ",temp->data);
			temp=temp->next;
		}
	}
}
void main()
{
	int choice;
	printf("Enter the elements of first list.\n");
	do
	{
		printf("\nEnter your choice.\n");
		printf("\n1.Enter element\n2.Traverse\n3.Enter elements of second list\n");
		scanf("%d",&choice);
		switch(choice){
			case 1:{
				start1=insertend(start1);
				break;
			}
			case 2:{
				traverse(start1);
				break;
			}
			case 3:{
				break;
			}
		}
	}while(choice!=3);
	printf("\nEnter the elements of second list.\n");
	do
	{
		printf("\n1.Enter element\n2.Traverse\n3.Exit\n");
		printf("\nEnter your choice.\n");
		scanf("%d",&choice);
		switch(choice){
			case 1:{
				start2=insertend(start2);
				break;
			}
			case 2:{
				traverse(start2);
				break;
			}
			case 3:{
				break;
			}
		}
	}while(choice!=3);
    nd *end;
    end=start1;
    while(end->next!=NULL)
	{
    	end=end->next;	
	}
	end->next=start2;
	printf("\nThe combined list is:");
	traverse(start1);
}
