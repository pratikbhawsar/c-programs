//34.Write a function which takes a matrix as an argument and return its transpose. 
#include<stdio.h>
void transf(int a[10][10],int r,int c)
   {
   	  int b[10][10];
		for(int i=0;i<r;i++)
		{
			for(int j=0;j<c;j++)
			{
				b[j][i]=a[i][j];
			}
		}
		printf("Transpose of matrix is:\n");
		for(int i=0;i<r;i++)
	    {
			for(int j=0;j<c;j++)
			{
				printf(" %d ",b[i][j]);
			}
				printf("\n");
		}		
   }
void main()
{
	int matrix[10][10],row,col;
	printf("Enter the no. of rows in the matrix.\n");
	scanf("%d",&row);
	printf("Enter the no. of columns in the matrix.\n");
	scanf("%d",&col);
	printf("Enter the elements of matrix:\n");
	for(int i=0;i<row;i++)
	{
		for(int j=0;j<col;j++)
		{
			scanf("%d",&matrix[i][j]);
		}
	}
	printf("\nMatrix is:\n");
	for(int i=0;i<row;i++)
	    {
			for(int j=0;j<col;j++)
			{
				printf(" %d ",matrix[i][j]);
			}
				printf("\n");
		}
	transf(matrix,row,col);
	
	
}
