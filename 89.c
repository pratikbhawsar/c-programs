//89.C program for Strong number.
#include<stdio.h>
int facto(int a)
{
	int temp,res=1;
	temp=a;
	while(temp>=1)
	{
	   res=res*temp;
	   temp--;
	}
	return res;
}
void main()
{
	int no,temp,num,sum=0,rem;
	printf("Enter the number.\n");
	scanf("%d",&no);
	num=no;
	while(no!=0)
	{
		rem=no%10;
		sum=sum+facto(rem);
		no=no/10;
	}
	if(sum==num)
	printf("Entered no. is an strong number.\n");
	else
	printf("Entered no. is not an strong number");
}
