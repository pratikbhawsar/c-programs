//85.Write a c program which deletes the duplicate element of an array.
#include<stdio.h>
void main()
{
	int a[50],len,newlen;
	printf("Enter the no. of elements in array.\n");
	scanf("%d",&len);
	newlen=len;
	printf("Enter the elements.\n");
	for(int i=0;i<len;i++)
	{
		scanf("%d",&a[i]);
	}
	printf("Array is:\n");
	for(int i=0;i<len;i++)
	{
		printf("%d ",a[i]);
	}
	for(int i=0;i<len;i++)
	{
		for(int j=i+1;j<=len;j++)
		{
			if(a[i]==a[j])
			{
				for(int k=j+1;k<=len;k++)
				{
					a[k-1]=a[k];	
				}
				len--;
			}
		}
	}
	printf("Array after deleting the duplicate elements is:\n");
	for(int i=0;i<len;i++)
	{
		printf("%d ",a[i]);
	}
}
