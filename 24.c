//24.Write a function to sort 1d integer array using Insertion sort.
#include<stdio.h>
void main()
{
	int a[100],len,temp,j;
	printf("Enter the no, of elements.\n");
	scanf("%d",&len);
	printf("Enter the elements.\n");
	for(int i=0;i<len;i++)
	{
		scanf("%d",&a[i]);
	}
	for(int i=1;i<len;i++)
	{
		temp=a[i];
		j=i-1;
		while(j>=0&&a[j]>temp)
		{
			a[j+1]=a[j];
			--j;
		}
	   a[j+1]=temp;
	}
	printf("Sorted elements :\n");
	for(int i=0;i<len;i++)
	{
		printf("%d\n",a[i]);
	}
}
