//6. Write a program to find the factorial of a given number using recursion.
#include<stdio.h>
unsigned int fact(int n);
int main()
 {
     int no,res;
     printf("Enter the number.");
     scanf("%d",&no);
     res=fact(no);
     printf("factorial of given no. is: %d",res);
     return 0;
 }
unsigned int fact(int n)
 {
     if(n!=1)
     return n*fact(n-1);
     else 
     return 1;
 }
