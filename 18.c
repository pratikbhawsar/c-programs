//18.Wap to implement linear search using recursion
#include<stdio.h>
#include<conio.h>
int linsearch(int arr[],int a,int b)
{
  if(arr[a]==b)
  {
  	return a;
  }
  else
  {
  	 if(a==10)
  	 return -1;
  	 linsearch(arr,a+1,b);	
  }
}
int main()
{
	int a[10]={1,5,53,67,32,72,31,16,9,45};
	int i,no,res;
	printf("Enter the element to be searched.\n");
	scanf("%d",&no);
	i=0;
	res=linsearch(a,i,no);
	if(res!=-1)
	printf("Element %d found at index:%d",no,res+1);
	else
	printf("Element not found.");
	return 0;
	
}
