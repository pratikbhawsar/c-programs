//28.Write a C function to generate Pascal's triangle.
#include<stdio.h>
void main()
{
	int no,n,res;
	printf("Enter the limit.\n");
	scanf("%d",&no);
	n=no;
	for(int i=0;i<no;i++)
	{
		for(int j=1;j<n;j++)
		{
			printf(" ");
		}n--;
		for(int k=i;k<=i;k++)
		{
			res=pow(11,k);
			printf("%d",res);
		}
		printf("\n");
	}
}
