//11.Write a function to interchange two integer values using call by reference technique.
#include<stdio.h>
#include<conio.h>
int swap(int* x,int* y)
{
   int temp;
   temp=*x;
   *x=*y;
   *y=temp;	
}
int main()
{
	int a,b;
	printf("Enter two numbers.\n");
	scanf("%d%d",&a,&b);
	printf("Before swapping : %d and %d\n",a,b);
	swap(&a,&b);
	printf("After swapping : %d and %d",a,b);
	return 0;
}
