//29.Write a function to convert a string into its opposite case.
#include<stdio.h>
void main()
{
	char str[100],len=0;
	printf("Enter the string.\n");
	gets(str);
	while(str[len]!='\0')
	{
		++len;
	}
	for(int i=0;i<len;i++)
	{
		if(str[i]>=65&&str[i]<=90)
		{
			str[i]+=32;
		}
		else if(str[i]>=97&&str[i]<=122)
		{
			str[i]-=32;
		}
	}
	printf("\n");
	puts(str);
}
