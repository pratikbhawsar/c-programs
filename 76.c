//76.Write a c program for scalar multiplication of matrix.
#include<stdio.h>
void scalarmul(int arr[10][10],int mul,int r,int c)
{
	for(int i=0;i<r;i++)
	{
		for(int j=0;j<c;j++)
		{
			arr[i][j]*=mul;
		}
	} 
	printf("\nThe scalar matrix multiplication is:\n");
	for(int i=0;i<r;i++)
	{
		for(int j=0;j<c;j++)
		{
			printf(" %d ",arr[i][j]);
		}
	   printf("\n");
	}
}
void main()
{
	int a[10][10],row,col,no;
	printf("Enter the number of rows in the matrix.\n");
	scanf("%d",&row);
	printf("Enter the number of columns in the matrix.\n");
	scanf("%d",&col);
	printf("Enter the elements.\n");
	for(int i=0;i<row;i++)
	{
		for (int j=0;j<col;j++)
		{
			scanf("%d",&a[i][j]);
		}
	}
	printf("Matrix is:\n");
	for(int i=0;i<row;i++)
	{
		for (int j=0;j<col;j++)
		{
			printf(" %d ",a[i][j]);
		}
	  printf("\n");
	}
	printf("Enter the number to multiply with matrix.\n");
	scanf("%d",&no);
	scalarmul(a,no,row,col);
	
}
