//58.	Write a C program to reverse the first n characters in a file.
// (Note: The file name and n are specified on the command line).
#include<stdio.h>
void main()
{
	FILE *fi;
	char str[500],fname[50],temp;
	int no,i=0,m=0;
	printf("Enter the filename.\n");
	gets(fname);
	printf("Enter the no. of characters to be reversed.\n");
	scanf("%d",&no);
	fi=fopen(fname,"r");	
	do
	{
		str[i]=fgetc(fi);
		i++;
	}
	while(str[i]!=EOF);
	fclose(fi);
	fi=fopen(fname,"w");
	printf("\ncontent is:\n %s",str);
	for(int j=0,k=no-1;j<k;j++,k--)
	{
	     temp=str[j];
		 str[j]=str[k];
		 str[k]=temp;	
	}
	printf("\ncontent is:\n %s",str);
    do
    {
       fputc(str[m],fi);
	   m++;	
	}while(str[m]!=EOF);
	printf("\nOperation successful.");
	
}
