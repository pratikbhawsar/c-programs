//12. Write a program to find the power (a, b) using recursion.
#include<stdio.h>
#include<conio.h>
int power(int a,int b)
  {
      
      if(b==0)
      return 1;
      if(b==1)
      return a;
      else
      {
          return a*power(a,b-1);
      }
  }
  int main()
  {
      int no,pow,result;
      printf("Enter the number and it's power.\n");
      scanf("%d %d",&no,&pow);
      result=power(no,pow);
      printf("Result is %d.",result);
      return 0;
  }
