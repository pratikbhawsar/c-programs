/*50.Write a C program to compute the monthly pay of 100 employees
using each employee's name, basic-pay. The DA is computed as 52% of the basic pay.
Gross-salary (Basic-pay+DA).Print the employees name and gross salary.*/
#include<stdio.h>
void main()
{
	struct employee{
		char name[30];
		int basicpay;
		int salary;
	};
	struct employee emp[100];
	for(int i=0;i<100;i++)
	{
		printf("Employee :%d\n",i+1);
		printf("Enter employee name.\n");
		scanf("%s",&emp[i].name);
		printf("Enter basic pay.\n");
		scanf("%d",&emp[i].basicpay);
		emp[i].salary=emp[i].basicpay+(.52*emp[i].basicpay);
	} 
	for(int i=0;i<100;i++)
	{
		printf("Employee name:%s\n",emp[i].name);
		printf("Gross salary:%d\n",emp[i].salary);
	}
	
}
