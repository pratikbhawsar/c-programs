/*15.Write a C program that uses functions to perform the following operations:
i.To insert a sub-string in to given main string from a given position.
ii.To delete n Characters from a given position in a given string.
iii.To replace a character of string either from beginning or ending or at a specified location.
*/
#include<stdio.h>
void main()
{
		char str[100],substr[100],choice;
		int pos,lsubstr=0,lstr=0,new_pos,n,lastpos,temp_len,ch; 
		printf("Enter the string.\n");
		gets(str); 
start:	printf("Enter the choice.\n1.Insert substring.\n2.Delete n characters.\n3.Replace a character.\n");
		scanf("%d",&choice);
		getchar();
		switch (choice)
		   {
			case 1:{
	    			printf("Enter the substring.\n");
					gets(substr);
					printf("Enter the position of string to insert the sub-string.\n");
					scanf("%d",&pos);
					--pos;                      // decrement position by 1 to match it with internal indexing.
					while(str[lstr]!='\0')
					{
						lstr++;                           // for calculating length of main string.
					} 
					while(substr[lsubstr]!='\0') 
					{
						lsubstr++;                       // for alculating length of sub-string
					}	
					new_pos=pos;
					for(int i=0;i<lsubstr;i++)
					{
						for(int j=lstr-1;j>=new_pos;j--)         // your position of element will change everytimr you shift the element which will be temporary.
						{
							str[j+1]=str[j];
						}
						++lstr;
						++new_pos;
					}
					for(int i=0,j=pos;i<lsubstr;i++,pos++)       // for inserting your substring into the main string.
					{
						str[pos]=substr[i];
					}
					printf("%s",str);
					break;
       			  }
 			case 2:{
 	   				printf("Enter the position from which you want to delete the elements.\n");
					scanf("%d",&pos);
					printf("Enter the no. of characters you want to delete.\n");
					scanf("%d",&n);
					--pos;
					while(str[lstr]!='\0')
					{
						++lstr;
					}
					lastpos=n+pos;
					temp_len=lstr;
					for(int i=0;i<n;i++)
					{
						for(int i=lastpos;i<=temp_len;i++)
						{
							str[i-1]=str[i];
						}
						--lastpos;
						--temp_len;
					}
					puts(str);
	  				break;
       			   }
       		case 3:{
       			    printf("Enter the position of character that you want to replace.\n");
					scanf("%d",&pos);
					getchar();
					printf("Enter the new character.\n");
					scanf("%c",&ch);
					--pos;
					str[pos]=ch;
					printf("The string is:\n%s",str);
				    break;
			       }
 			default:
     				{
        			    printf("Invalid choice, enter again.\n");
       				    goto start;			 		
	   				}			
   	       }
}
