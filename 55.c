//55.Write a program to copy the contents of one file into another file.
#include<stdio.h>
void main()
{
	FILE *fi1,*fi2;
	char ch;
	int i=0;
	fi1=fopen("one.txt","r");
	fi2=fopen("two.txt","w");
	if(fi1)
	{
		do
		{
		    ch=fgetc(fi1);
	     	if(ch=='\0')
	     	break;
	     	fputc(ch,fi2);
		}
		while(ch!=EOF);
        fclose(fi1);
        fclose(fi2);
		printf("The contents of the file copied successfully.");
	}
	else
	{
		printf("File not found.");
	}
}
