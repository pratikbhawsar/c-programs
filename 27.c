//27.Write a program to find the smallest and largest element in a two dimensional array.
#include<stdio.h>
#include<conio.h>
int main()
{
    int a[10][10],row,col,min,max;
    printf("Enter the no. of rows.\n");
    scanf("%d",&row);
    printf("Enter the no. of columns.\n");
    scanf("%d",&col);
    printf("Enter the elements.\n");
    for(int i=0; i<row; i++)
    {
        for(int j=0; j<col; j++)
        {
            scanf("%d",&a[i][j]);
        }
    }
    min=a[0][0];
    max=a[0][0];
    printf("\n");
    for(int i=0;i<row;i++)
    {
        for(int j=0;j<col;j++)
        {
            printf("%d ",a[i][j]);
        }
        printf("\n");
    }
    for(int i=0;i<row;i++)
    {
        for(int j=0;j<col;j++)
        {
            if(a[i][j]<min)
            {
                min=a[i][j];
            }
        }
    }
    for(int i=0;i<row;i++)
    {
        for(int j=0;j<col;j++)
        {
            if(a[i][j]>max)
            {
              max=a[i][j];
            }
        }
    }
    printf("\n");
    printf("Largest element is: %d\n",max);
    printf("Smallest element is: %d",min);
    return 0;
}
