//71.Write a c program for addition of two matrices.
#include<stdio.h>
void main()
{
    int a[10][10],b[10][10],res[10][10],row,col;
    printf("Enter the no. of rows.\n");
    scanf("%d",&row);
    printf("Enter the no. of columns.\n");
    scanf("%d",&col);
    printf("Enter elements of first matrix.\n");
    for(int i=0;i<row;i++)
    {
        for(int j=0;j<col;j++)
        {
            scanf("%d",&a[i][j]);
        }
    }
    printf("\nEnter elememts of second matrix.\n");
    for(int i=0;i<row;i++)
    {
        for(int j=0;j<col;j++)
        {
            scanf("%d",&b[i][j]);
        }
    }
    for(int i=0;i<row;i++)
    {
        for(int j=0;j<col;j++)
        {
           res[i][j]=a[i][j]+b[i][j]; 
        }
    }
    printf("\nMatrix addition is:\n");
    for(int i=0;i<row;i++)
    {
        for(int j=0;j<col;j++)
        {
            printf("%d ",res[i][j]);
        }
        printf("\n");
    }
}
