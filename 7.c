//7.Write a program to print the Fibonacci series using recursion.
#include<stdio.h>
int fibbo(int n)
 {
     if(n==1)
     {
         return 0;
     }
     if(n==2)
     {
         return 1;
     }
     else 
     {
         return fibbo(n-1)+fibbo(n-2);
     }
 }
 int main()
 {
     int no,i;
     printf("Enter the terms you want.");
     scanf("%d",&no);
     for(i=1;i<=no;i++)
     {
         printf(" %d ",fibbo(i));
     }
     return 0;
 }
