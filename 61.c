//61. Write a program to implement stack operations using Arrays.
#include<stdio.h>
#include<stdlib.h>
int stack[5],top=-1;
int item;
void push()
{
	if(top==4)
	{
		printf("\nStack is full.");
    }
    else
    {
		printf("\nEnter the element to be inserted.\n");
		scanf("%d",&item);
	   	top=top+1;
	   	stack[top]=item;
	}
}
void pop()
{
	if(top==-1)
	{
		printf("\nStack is empty.\n");
	}
	else
	{
	printf("\nThe element deleted is: %d",stack[top]);
	top=top-1;
    }
}
void traverse()
{
	if(top==-1)
	{
		printf("Stack is empty.\n");
	}
	else
	{
	printf("Stack currently has:");
		for(int i=top;i>=0;i--)
		{
			printf("%d ",stack[i]);
		}
	    printf("\n");
    }
}
void main()
{
	int choice;
	do
	{
    printf("\n1.Insert\n2.Delete\n3.Traverse\n4.exit\n");
	printf("\nEnter your choice\n");
	scanf("%d",&choice);
		switch(choice){
			case 1:{
				push();
				break;
			}
			case 2:{
				pop();
				break;
			}
			case 3:{
				traverse();
				break;
			}
			case 4:{
				exit(0);
				break;
			}		
	   }
   }while(choice!=4);
}
