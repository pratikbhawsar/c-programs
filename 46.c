/*46.Define a structure with the name complex which contains rael part and imaginary part.
write the functions to add, subtract and multiply two complex numbers 
using returning as an complex number.*/
#include<stdio.h>
struct complex{
		int real;
		int img;
	}c1,c2,c3;
struct complex add(struct complex a,struct complex b)
{
	struct complex c ;
	c.real=a.real+b.real;
	c.img=a.img+b.img;
	return c;
}
struct complex sub(struct complex a,struct complex b)
{
	struct complex c ;
	c.real=a.real-b.real;
	c.img=a.img-b.img;
	return c;
}
struct complex mul(struct complex a,struct complex b)
{
	struct complex c ;
	c.real=(a.real * b.real) - (a.img * b.img);
    c.img=(a.real * b.img) + (a.img * b.real);
	return c;
}
void main()
{

     	printf("Enter two complex numbers.");
		printf("\nReal part: ");
		scanf("%d",&c1.real);
		printf("\nImaginary part: ");
		scanf("%d",&c1.img);
		printf("\nReal part: ");
		scanf("%d",&c2.real);
		printf("\nImaginary part: ");
		scanf("%d",&c2.img);
 	    c3=add(c1,c2);
 	    if(c3.img>=0)
 	    printf("Addition is %d+%di.\n",c3.real,c3.img);
 	    else
 	    printf("Addition is %d%di.\n",c3.real,c3.img);
 	    c3=sub(c1,c2);
 	    if(c3.img>=0)
 	    printf("Substraction is %d+%di.\n",c3.real,c3.img);
 	    else
 	    printf("Substraction is %d%di.\n",c3.real,c3.img);
 	    c3=mul(c1,c2);	
 	    if(c3.img>=0)
 	    printf("Multiplication is %d+%di.\n",c3.real,c3.img);
 	    else
 	    printf("Multiplication is %d%di.\n",c3.real,c3.img);		 	
}
