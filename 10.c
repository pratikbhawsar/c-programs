//10. Write a function to interchange two integer values using call-by-value technique.
#include<stdio.h>
#include<conio.h>
int swap(int a, int b)
  {
      int temp;
      temp=a;
      a=b;
      b=temp;
      printf("Number after swapping: %d and %d",a,b);      
  }
  int main()
  {
      int a,b;
      printf("Enter two numbers to interchange:\n ");
      scanf("%d%d",&a,&b);
      printf("Numbers before swapping: %d and %d.\n",a,b);
      swap(a,b);
      return 0;
  }
