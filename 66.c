//66.Write a program to perform Doubly Linked List Operation.
//(create,insert,delete,traversing).
#include<stdio.h>
#include<stdlib.h>
struct node{
	int data;
	struct node *prev;
	struct node *next;
};
typedef struct node nd;
nd *start=NULL,*end=NULL;
void insertatend()
{
	nd *temp;
	int item;
	temp=(nd*)malloc(sizeof(nd));
	printf("\nEnter the item.\n");
	scanf("%d",&item);
	temp->data=item;
	if(start==NULL)
	{
		temp->prev=NULL;
		temp->next=NULL;
		start=temp;
		end=temp;		
	}
	else
	{
		temp->prev=end;
		temp->next=NULL;
		end->next=temp;
		end=temp;
	}
}
void insertatbegin()
{
	nd *temp;
	int item;
	temp=(nd*)malloc(sizeof(nd));
	printf("\nEnter the item.\n");
	scanf("%d",&item);
	temp->data=item;
	if(start==NULL)
	{
		temp->next=NULL;
		temp->prev=NULL;
		start=temp;
		end=temp;
	}
	else
	{
		temp->prev=NULL;
		temp->next=start;
		start->prev=temp;		
		start=temp;
	}
}
void deleteend()
{
	int item;
	if(start==NULL)
	{
		printf("\nQueue is empty.");
	}
	else
	{
		item=end->data;
		if(end->prev==NULL)
		{
			start=NULL;
			end=NULL;
		}
		else
		{
		end=end->prev;
		end->next=NULL;		
		}
		printf("\nItem deleted is %d.\n",item);		
	}
}
void deletebegin()
{
	int item;
	if(start==NULL)
	{
		printf("\nQueue is empty.\n");
	}
	else
	{
		item=start->data;
		if(start->next==NULL)
		{
			start=NULL;
			end=NULL;
		}
		else
		{
			start=start->next;
			start->prev=NULL;
		}
		printf("\nItem deleted is %d.\n",item);	
	}
	
}
void traverse()
{
	nd *temp;
	temp=start;
	if(start!=NULL)
	{
		printf("Currently list has:");
		while(temp!=NULL)
		{
			printf("%d ",temp->data);
			temp=temp->next;
		}
	}
	else
	{
		printf("\nList is empty");
	}
}
void main()
{
	int choice;
	do
	{
		printf("\n1.Insert at end.");
		printf("\n2.Insert at begin.");
		printf("\n3.Delete from end.");
		printf("\n4.Delete from begin.");
		printf("\n5.Traverse.");
		printf("\n6.Exit.");
		printf("\nEnter your choice:");
		scanf("%d",&choice);
		switch(choice){
			case 1:{
				insertatend();
				break;
			}
			case 2:{
				insertatbegin();
				break;
			}
			case 3:{
				deleteend();
				break;
			}
			case 4:{
				deletebegin();
				break;
			}
			case 5:{
				traverse();
				break;
			}
			case 6:{
				exit(0);
				break;
			}
		}
	}while(choice!=6);
}
